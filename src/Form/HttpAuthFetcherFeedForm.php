<?php

namespace Drupal\cmrf_feeds\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form on the feed edit page for the cmrf_feeds_apiv3 plugin.
 */
class HttpAuthFetcherFeedForm extends ExternalPluginFormBase implements ContainerInjectionInterface {

  /**
   * Constructs an HttpFeedForm object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    ClientInterface $client,
    protected readonly EntityTypeManagerInterface $entityTypeManager
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FeedInterface $feed = NULL) {
    $configuration = $feed->getConfigurationFor($this->plugin);
    $form['connection_id'] = [
      '#title' => $this->t('CiviMRF Connector'),
      '#type' => 'select',
      '#options' => $this->getOptionsForConnectors(),
      '#default_value' => $configuration['connection_id'],
      '#required' => TRUE,
    ];
    $form['entity'] = [
      '#title' => $this->t('Entity'),
      '#type' => 'textfield',
      '#default_value' => $configuration['entity'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, FeedInterface $feed = NULL) {
    parent::submitConfigurationForm($form, $form_state, $feed);

    $configuration = $feed->getConfigurationFor($this->plugin);
    $configuration['connection_id'] = $form_state->getValue('connection_id');
    $configuration['entity'] = $form_state->getValue('entity');
    $feed->setConfigurationFor($this->plugin, $configuration);
  }

  /**
   * Builds an options array for cmrf_connectors.
   *
   * @return array
   *   An options array.
   */
  private function getOptionsForConnectors(): array {
    $options = [];
    $connectors = $this->entityTypeManager->getStorage('cmrf_connector')->loadMultiple();
    foreach ($connectors as $connector) {
      $options[$connector->id()] = $connector->label();
    }

    return $options;
  }

}
