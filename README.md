# CMRF Feeds

Integrate CMRF connectors with the feeds module as a fetcher.

### Features

- Import and map entities from CiviCRM to Drupal entities using the feeds module.
- Seamless integration with the existing CMRF ecosystem.

### Additional Requirements

- [CMRF Core](https://www.drupal.org/project/cmrf_core)
- [Feeds](https://www.drupal.org/project/feeds)

### Known Limitations

As of our current implementation based on our initial use-case, we support only API V3. Contributions for V4 integration are welcome.

### Configuration

1. Install the module in the usual manner.
2. Configure a CiviMRF Connector and Profile at `admin/config/cmrf`.
3. Create a new feed type at `admin/structure/feeds`.
4. Choose "Download from Civi APIv3" as your fetcher.
5. Select JsonPath as your parser.
6. Configure your mapping and use `$.values.[].*` as the context.
