<?php

namespace Drupal\cmrf_feeds\Feeds\Fetcher;

use CMRF\Core\Core;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\feeds\File\FeedsFileSystemInterface;
use Drupal\feeds\Plugin\Type\ClearableInterface;
use Drupal\feeds\Plugin\Type\Fetcher\FetcherInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds\Result\FetcherResult;
use Drupal\feeds\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\feeds\FeedInterface;

/**
 * Defines feeds fetcher based on cmrf_core using api v3.
 *
 * @FeedsFetcher(
 *   id = "cmrf_feeds_apiv3",
 *   title = @Translation("Download from Civi APIv3"),
 *   description = @Translation("Downloads data from CiviCRM using the apiv3."),
 *   form = {
 *     "configuration" = "Drupal\feeds\Feeds\Fetcher\Form\HttpFetcherForm",
 *     "feed" = "Drupal\cmrf_feeds\Form\HttpAuthFetcherFeedForm",
 *   }
 * )
 */
class CMRFFeedsAPIV3 extends PluginBase implements ClearableInterface, FetcherInterface, ContainerFactoryPluginInterface {

  /**
   * Constructs an UploadFetcher object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\feeds\File\FeedsFileSystemInterface $feedsFileSystem
   *   Feeds file system helper for Feeds.
   * @param \Drupal\cmrf_core\Core $CMRFCore
   *   The cmrf_core service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    protected readonly FeedsFileSystemInterface $feedsFileSystem,
    protected readonly Core $cmrfCore
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('feeds.file_system.in_progress'),
      $container->get('cmrf_core.core')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultFeedConfiguration() {
    return [
        'connection_id' => '',
        'entity' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state) {
    $configuration = $feed->getConfigurationFor($this);

    // Check if essential configuration is set.
    if (empty($configuration['connection_id']) || empty($configuration['entity'])) {
      throw new \Exception('Missing essential configuration for CMRFFeedsAPIV3.');
    }

    // Get data from cmrf and wirte it to a file.
    $call = $this->cmrfCore->createCall($configuration['connection_id'], $configuration['entity'], 'get', [], ['limit' => 0]);
    $this->cmrfCore->executeCall($call);
    $response = $call->getReply();

    $sink = $this->feedsFileSystem->saveData(JSON::encode($response), "cmrf_feeds_apiv3_{$feed->id()}");
    return new FetcherResult($sink);
  }

  /**
   * {@inheritdoc}
   */
  public function clear(FeedInterface $feed, StateInterface $state) {
    $this->feedsFileSystem->removeFiles($feed->id());
  }

}
